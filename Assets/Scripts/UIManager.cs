﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public Image dashImage;
    public Image weaponImage;
    public Slider playerHealth;
    public PlayerUnit player;
    public GameObject youDiedObj;

    [Header("Pause")]
    public GameObject pauseObj;
    public Toggle muteToggle;

    [Header("LevelInfo")]
    public GameObject loadLevelInfoObj;
    public GameObject leaveLevelObj;
    public TextMeshProUGUI levelName;

    bool isPaused = false;
    bool canPause = true;

    float dashTotalTime = 0;
    float dashCount = 0;
    bool dashCounting = false;

    GameManager gameManager;

    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }

    private void Update()
    {
        if(dashCounting)
        {
            dashCount += Time.deltaTime;
            if (dashCount >= dashTotalTime)
            {
                dashCount = dashTotalTime;
                dashCounting = false;
            }

            SetDashImageFill(dashCount / dashTotalTime);
        }

        if(player != null)
        {
            if(playerHealth != null)
            {
                float h = player.currentHealth / player.maxHealth;
                playerHealth.value = h;
            }
        }

        weaponImage.sprite = player.mainWeapon.weaponSprite;

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (canPause)
            {
                PauseGame();
            }
            else
            {
                if (loadLevelInfoObj.activeInHierarchy)
                    ShowLevelInfo(false);
                if (leaveLevelObj.activeInHierarchy)
                    ShowLeaveLevel(false);                
            }
        }
    }

    void PauseGame()
    {
        isPaused = !isPaused;
        gameManager.PauseGame(isPaused);
        pauseObj.SetActive(isPaused);
    }

    public void ResetDashFill(float time)
    {
        dashTotalTime = time;
        dashCount = 0;
        dashCounting = true;
    }

    public void ZeroDashFill()
    {
        SetDashImageFill(0);
    }

    void SetDashImageFill(float value)
    {
        if(dashImage != null)
        {
            dashImage.fillAmount = value;
        }
    }

    public void MuteButton()
    {
        gameManager.audioManager.MuteAudio(muteToggle.isOn);
    }

    public void QuitGame()
    {
        gameManager.QuitGame();
    }

    public void MainMenu()
    {
        gameManager.LoadScene("Title_MainMenu");
    }

    public void ShowLeaveLevel(bool value)
    {
        if (leaveLevelObj != null)
        {
            leaveLevelObj.SetActive(value);
            canPause = !value;

            if (!value)
                gameManager.PauseGame(false);
        }
    }

    public void ShowLevelInfo(bool value)
    {
        if (loadLevelInfoObj != null)
        {
            loadLevelInfoObj.SetActive(value);
            canPause = !value;

            if(!value)
                gameManager.PauseGame(false);
        }
    }

    public void ShowYouDied(bool value)
    {
        if(youDiedObj != null)
        {
            youDiedObj.SetActive(value);

            if (!value)
                gameManager.PauseGame(false);
        }
    }
}
