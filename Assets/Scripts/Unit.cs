﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Unit : MonoBehaviour
{
    protected GameManager gameManager;
    public float maxHealth = 5;
    public float currentHealth = 5;
    public float defense = 0;
    public float baseAttack = 0;
    public float speed = 1;

    public bool isAlive = true;
    public GameObject deathFx;
    protected GameObject createdDeathFx;

    public BoxCollider attackDetection;

    protected NavMeshAgent navMesh;

    public Transform bodyLimit;

    protected UnitAnimation unitAnimation = new UnitAnimation();

    public GameObject weaponGO;
    public Weapon mainWeapon;
    protected Animation weaponAnim;
    protected bool canAttack = true;

    protected List<Unit> unitsInsideRange = new List<Unit>();
    public string unityEnemyTag = "Enemy";

    protected Vector3 startPosition;
    bool positionSet = false;
    protected Quaternion startRotation;
    bool rotationSet = false;

    Vector3 deathPosition;
    Quaternion deathRotation;

    public float invincibilyTime = 1;
    float invTimer = 0;
    protected bool isInvicible = false;

    bool isDying = false;

    public void UnitStart(bool revive = false)
    {
        if (gameManager == null)
            gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

        if (!revive)
        {
            if (positionSet)
            {
                gameObject.transform.position = startPosition;
            }
            else
            {
                startPosition = gameObject.transform.position;
                positionSet = true;
            }

            if (rotationSet)
            {
                gameObject.transform.rotation = startRotation;
            }
            else
            {
                startRotation = gameObject.transform.rotation;
                rotationSet = true;
            }
        }
        else
        {
            transform.position = deathPosition;
            transform.rotation = deathRotation;
        }

        isDying = false;
        currentHealth = maxHealth;
        isAlive = true;        
        float atkRange = mainWeapon != null ? mainWeapon.attackRange : 0;
        attackDetection.size = new Vector3(attackDetection.size.x, attackDetection.size.y, atkRange);
        attackDetection.center = new Vector3(0, attackDetection.center.y, bodyLimit.localPosition.z + attackDetection.size.z / 2);        

        if (mainWeapon != null)
        {
            if (mainWeapon.weaponPrefab != null && weaponGO == null)
            {
                weaponGO = Instantiate(mainWeapon.weaponPrefab);
                weaponGO.transform.SetParent(transform);
                weaponGO.transform.rotation = transform.rotation;
                weaponGO.transform.localPosition = Vector3.zero;
                weaponAnim = weaponGO.GetComponentInChildren<Animation>();
            }
        }

        if(unitAnimation.unitAnimator == null)
            unitAnimation.unitAnimator = GetComponentInChildren<Animator>();

        unitAnimation.ResetAnimation();

        if (navMesh != null)
            navMesh.enabled = true;

        unitsInsideRange.Clear();
        unitsInsideRange.TrimExcess();
    }

    public void UnitUpdate()
    {
        if(currentHealth <= 0)
        {
            currentHealth = 0;
            isAlive = false;
            if(!isDying)
            Die();
        }
        unitAnimation.isAlive = isAlive;
        unitAnimation.Update();

        if(isInvicible)
        {
            invTimer += Time.deltaTime;
            if(invTimer >= invincibilyTime)
            {
                isInvicible = false;
            }
        }

        if(navMesh != null)
            navMesh.speed = speed;
    }

    void Die()
    {
        isDying = true;

        if(deathFx != null && createdDeathFx == null)
        {
            createdDeathFx = Instantiate(deathFx,transform);
            Vector3 angle = createdDeathFx.transform.eulerAngles;
            angle.x = -90;
            createdDeathFx.transform.eulerAngles = angle;
            createdDeathFx.transform.localPosition = new Vector3(0,.37f,-.75f);
        }

        if (weaponGO != null)
            Destroy(weaponGO);

        isInvicible = false;

        unitAnimation.die = true;

        deathPosition = transform.position;
        deathRotation = transform.rotation;

        unitsInsideRange.Clear();
        unitsInsideRange.TrimExcess();
    }

    public void ReceiveDamage(float amount)
    {
        if (!isInvicible)
        {
            currentHealth -= (amount - defense);
            isInvicible = true;
            invTimer = 0;
        }

        UnitUpdate();
    }

    public void RemoveDeadEnemyFromList(Unit enemy)
    {
        unitsInsideRange.Remove(enemy);
    }

    public void AttackBase()
    {
        canAttack = false;
        unitAnimation.attack = true;
        StartCoroutine(WaitAttack());
    }

    IEnumerator WaitAttack()
    {
        yield return new WaitForSeconds(mainWeapon.attackRate);
        canAttack = true;
    }

    public void ResetUnit()
    {
        gameObject.SetActive(true);
        if (createdDeathFx != null)
            DestroyImmediate(createdDeathFx);
        UnitStart(false);
    }

    public void Revive()
    {
        gameObject.SetActive(true);
        if (createdDeathFx != null)
            DestroyImmediate(createdDeathFx);
        UnitStart(true);        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == unityEnemyTag)
        {
            unitsInsideRange.Add(other.gameObject.GetComponent<Unit>());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == unityEnemyTag)
        {
            unitsInsideRange.Remove(other.gameObject.GetComponent<Unit>());
        }
    }
}