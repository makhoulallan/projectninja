﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAnimation
{
    public bool attack = false;
    public bool isAlive = false;
    public bool die = false;
    public Animator unitAnimator;

    public void Update()
    {
        if (unitAnimator != null)
        {
            if (attack)
            {
                unitAnimator.SetTrigger("attack");
                attack = false;
            }

            if (die)
            {
                unitAnimator.SetTrigger("die");
                die = false;
            }

            unitAnimator.SetBool("isAlive", isAlive);
        }
    }

    public void ResetAnimation()
    {
        attack = false;
        die = false;
        isAlive = true;
    }
}
