﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Villager : Unit
{
    public PlayerUnit player;
    BoxCollider bCollider;
    public float respawnTime = 2;
    bool dying = false;

    public bool canBePushed = true;
    public bool chasePlayer = true;
    public bool attackPlayer = true;
    public bool respawn = false;
    public bool duplicate = false;

    GameObject thisObject;

    private void Start()
    {
        bCollider = GetComponent<BoxCollider>();
        UnitStart();
        navMesh = GetComponent<NavMeshAgent>();

        player = gameManager.player.GetComponent<PlayerUnit>();

        thisObject = gameObject;
    }

    private void Update()
    {
        UnitUpdate();

        if(!isAlive && !dying)
        {
            Die();
        }

        if (isAlive && chasePlayer)
        {
            if(navMesh == null)
            {
                navMesh = GetComponent<NavMeshAgent>();
            }

            if (!player.player.IsDashing)
            {
                navMesh.isStopped = false;
                navMesh.SetDestination(player.transform.position);
            }
            else
            {
                navMesh.isStopped = true;
            }
        }

        if (unitsInsideRange.Count > 0)
        {
            if(attackPlayer)
                Attack();            
        }

        if(!canBePushed)
        {
            transform.position = startPosition;
            transform.rotation = startRotation;
        }
    }

    void Attack()
    {
        if (canAttack && isAlive)
        {
            if (weaponAnim != null)
            {
                weaponAnim.clip = mainWeapon.weaponAnimation;
                weaponAnim.Play();
            }

            player.ReceiveDamage(baseAttack + mainWeapon.damage);

            AttackBase();
        }
    }

    void Die()
    {
        dying = true;
        navMesh.enabled = false;        
        bCollider.enabled = false;
        player.RemoveDeadEnemyFromList(this);
        Rigidbody rB = GetComponent<Rigidbody>();
        rB.velocity = Vector3.zero;
        rB.angularVelocity = Vector3.zero;

        if(respawn)
            StartCoroutine(RespawnWait());
    }

    void Respawn()
    {        
        bCollider.enabled = true;
        Revive();
        dying = false;
        navMesh.enabled = true;

        if(duplicate)
        {
            Instantiate(thisObject, transform.position, transform.rotation, transform.parent);
        }        
    }

    IEnumerator RespawnWait()
    {
        yield return new WaitForSeconds(respawnTime);
        Respawn();
    }
}
