﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Fighter_BOSS : Unit
{
    public PlayerUnit player;
    BoxCollider bCollider;
    bool dying = false;

    public bool canBePushed = true;
    public bool chasePlayer = true;
    public bool attackPlayer = true;

    private void Start()
    {
        bCollider = GetComponent<BoxCollider>();
        UnitStart();
        navMesh = GetComponent<NavMeshAgent>();

        player = gameManager.player.GetComponent<PlayerUnit>();
    }

    private void Update()
    {
        UnitUpdate();

        if(!isAlive && !dying)
        {
            Die();
        }

        if (isAlive && chasePlayer)
        {
            if (!player.player.IsDashing)
            {
                navMesh.isStopped = false;
                navMesh.SetDestination(player.transform.position);
            }
            else
            {
                navMesh.isStopped = true;
            }
        }

        if(unitsInsideRange.Count > 0)
        {
            if(attackPlayer)
                Attack();            
        }

        if(!canBePushed)
        {
            transform.position = startPosition;
            transform.rotation = startRotation;
        }
    }

    void Attack()
    {
        if (canAttack && isAlive)
        {
            if (weaponAnim != null)
            {
                weaponAnim.clip = mainWeapon.weaponAnimation;
                weaponAnim.Play();
            }

            player.ReceiveDamage(baseAttack + mainWeapon.damage);

            AttackBase();
        }
    }

    void Die()
    {
        dying = true;
        navMesh.enabled = false;        
        bCollider.enabled = false;
        player.RemoveDeadEnemyFromList(this);
        Rigidbody rB = GetComponent<Rigidbody>();
        rB.velocity = Vector3.zero;
        rB.angularVelocity = Vector3.zero;
    }
}
