﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    public UIManager uiManager;
    GameManager gameManager;
    [HideInInspector] public PlayerUnit pUnit;

    public float speed = 10;

    //DASH
    public float dashSpeed = 30;
    public float dashDistance = 4;
    public float dashWait = 2;
    bool dashEnabled = true;
    bool dashMove = false;
    Vector3 dashStartPos;
    Vector3 dashDir;
    public float dashDamage = 1;
    public GameObject dashFx;

    public bool IsDashing
    {
        get { return dashMove; }
    }

    Vector2 moveDir;
    Vector2 lookDir;

    bool enableMovement = true;

    Rigidbody rB;

    void Start()
    {
        rB = GetComponent<Rigidbody>();
        pUnit = GetComponent<PlayerUnit>();
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }

    void Update()
    {
        if(enableMovement)
            MovePlayer();

        LookAt();

        if (dashMove)
            DashMove();

        pUnit.UnitUpdate();

        CheckDeath();
    }

    void CheckDeath()
    {
        if(!pUnit.isAlive)
        {
            uiManager.ShowYouDied(true);            
            pUnit.ResetUnit();
            gameManager.LoadLevel(true);
            gameManager.PauseGame(true);
        }
    }

    void MovePlayer()
    {
        Vector3 pos = transform.position;
        pos.x += (moveDir.x * speed * Time.deltaTime);
        pos.z += (moveDir.y * speed * Time.deltaTime);
        transform.position = pos;
    }

    void LookAt()
    {
        RaycastHit rayHit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out rayHit, 200))
        {
            lookDir.x = rayHit.point.x;
            lookDir.y = rayHit.point.z;
        }

        transform.LookAt(new Vector3(lookDir.x, transform.position.y, lookDir.y));        
    }

    void Dash()
    {
        if (dashEnabled)
        {
            if (moveDir.magnitude > 0.1f)
            {
                if (dashFx != null)
                {
                    Instantiate(dashFx, transform.position, transform.rotation);
                }

                HideShowUnit(false);

                enableMovement = false;
                dashEnabled = false;

                rB.velocity = Vector3.zero;

                dashDir = transform.position;
                dashDir.x = moveDir.x;
                dashDir.z = moveDir.y;

                dashStartPos = transform.position;

                dashMove = true;
                pUnit.SetInvincibility(true);

                uiManager.ZeroDashFill();
            }
        }
    }

    void HideShowUnit(bool show)
    {
        MeshRenderer[] meshR = GetComponentsInChildren<MeshRenderer>();
        SkinnedMeshRenderer[] sMeshR = GetComponentsInChildren<SkinnedMeshRenderer>();

        foreach (var m in meshR)
            m.enabled = show;

        foreach (var m in sMeshR)
            m.enabled = show;
    }

    private void OnCollisionStay(Collision collision)
    {
        CollisionCheck(collision);
    }

    Vector3 colPos;
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Wall")
            colPos = transform.position;

        CollisionCheck(collision);
    }

    void CollisionCheck(Collision collision)
    {
        if (IsDashing)
        {
            if (collision.gameObject.tag == "Wall")
            {
                Vector3 contP = collision.GetContact(0).point;
                contP.y = transform.position.y;
                transform.position = colPos;
                dashMove = false;
                pUnit.SetInvincibility(false);
                enableMovement = true;
                HideShowUnit(true);
                StartCoroutine(WaitDash());
            }

            if (collision.gameObject.tag == "Enemy")
            {
                Unit enemy = collision.gameObject.GetComponent<Unit>();
                enemy.ReceiveDamage(dashDamage);
            }
        }
    }

    void DashMove()
    {
        Vector3 dash = transform.position;
        dash += (dashDir * dashSpeed * Time.deltaTime);
        dash.y = transform.position.y;
        transform.position = dash;

        if(Vector3.Distance(transform.position, dashStartPos) >= dashDistance)
        {
            dashMove = false;
            pUnit.SetInvincibility(false);
            enableMovement = true;
            HideShowUnit(true);
            StartCoroutine(WaitDash());
        }
    }

    IEnumerator WaitDash()
    {
        uiManager.ResetDashFill(dashWait);
        yield return new WaitForSeconds(dashWait);
        dashEnabled = true;
    }

    public void OnMove(InputValue value)
    {
        Vector2 mDir = value.Get<Vector2>();
        Vector3 mDir3 = new Vector3(mDir.x, 0, mDir.y);
        Vector3 moveDir3 = Camera.main.transform.TransformDirection(mDir3);
        moveDir.x = moveDir3.x;
        moveDir.y = moveDir3.z;
    }

    public void OnLook(InputValue value)
    {
        /*
        RaycastHit rayHit;
        Ray ray = Camera.main.ScreenPointToRay(value.Get<Vector2>());

        if (Physics.Raycast(ray, out rayHit, 200))
        {
            lookDir = rayHit.point;
        }
        */
    }

    public void OnAttack()
    {
        pUnit.Attack();
    }

    public void OnDash()
    {
        Dash();
    }

    public void OnSwitchWeapons()
    {
        pUnit.SwitchWeapons();
    }

    public void OnInteract()
    {
        Debug.Log("Interact");
    }
}
