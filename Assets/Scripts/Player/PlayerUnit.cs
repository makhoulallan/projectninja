﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUnit : Unit
{
    public Player player;
    public Weapon secondaryWeapon;
    public bool secondaryWeaponEnabled = false;

    private void Start()
    {
        UnitStart();
    }

    private void Update()
    {
        speed = player.speed;
        UnitUpdate();        
    }

    public void SwitchWeapons()
    {
        if (secondaryWeaponEnabled)
        {
            Weapon temp = secondaryWeapon;
            secondaryWeapon = mainWeapon;
            mainWeapon = temp;

            if (weaponGO != null)
                Destroy(weaponGO);

            if (mainWeapon.weaponPrefab != null && mainWeapon.isCloseRange)
            {
                weaponGO = Instantiate(mainWeapon.weaponPrefab);
                weaponGO.transform.SetParent(transform);
                weaponGO.transform.rotation = transform.rotation;
                weaponGO.transform.localPosition = Vector3.zero;
                weaponAnim = weaponGO.GetComponentInChildren<Animation>();
            }

            float atkRange = mainWeapon != null ? mainWeapon.attackRange : 0;
            attackDetection.size = new Vector3(attackDetection.size.x, attackDetection.size.y, atkRange);
            attackDetection.center = new Vector3(0, attackDetection.center.y, bodyLimit.localPosition.z + attackDetection.size.z / 2);
        }
    }

    public void Attack()
    {
        if (canAttack)
        {
            if (mainWeapon.isCloseRange)
            {
                if (weaponAnim != null)
                {
                    weaponAnim.clip = mainWeapon.weaponAnimation;
                    weaponAnim.Play();
                }

                foreach (var u in unitsInsideRange)
                {
                    float d = mainWeapon != null ? mainWeapon.damage : 1;
                    u.ReceiveDamage(baseAttack + d);
                }
            }
            else
            {
                weaponGO = Instantiate(mainWeapon.weaponPrefab);
                weaponGO.transform.rotation = transform.rotation;
                Vector3 sPos = (attackDetection.transform.position - transform.position) + attackDetection.center;
                sPos.z -= attackDetection.size.z/2;
                weaponGO.transform.position = transform.TransformPoint(sPos);
                weaponAnim = weaponGO.GetComponentInChildren<Animation>();
                Projectile p = weaponGO.GetComponentInChildren<Projectile>();
                p.direction = transform.forward;
                p.distance = mainWeapon.attackRange;
                p.damage = mainWeapon.damage;
            }

            AttackBase();
        }
    }

    public void SetInvincibility(bool value)
    {
        isInvicible = value;
    }
}
