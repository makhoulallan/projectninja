﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public string levelName = "";
    public Transform startPosition;
    Unit[] allUnits;

    private bool isFinished = false;
    public bool IsFinished
    {
        get
        {
            isFinished = CheckAllUnitsDead();
            return isFinished;
        }
    }

    void Start()
    {
        allUnits = GetComponentsInChildren<Unit>();
    }

    void Update()
    {
        
    }

    bool CheckAllUnitsDead()
    {
        foreach(var u in allUnits)
        {
            if (u.isAlive)
                return false;
        }

        return true;
    }

    public void ResetLevel()
    {
        foreach(var u in allUnits)
        {
            u.ResetUnit();
        }
    }
}
