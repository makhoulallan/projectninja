﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject titleObj;
    public GameObject menuObj;
    public GameObject controlsObj;
    public GameObject loadingScreenPrefab;
    LoadingScreen loadingScreenCreated;
    bool titleShowing = true;

    void Start()
    {

    }

    void Update()
    {
        if(titleShowing)
        {
            if(Input.anyKeyDown)
            {
                ShowTitle(false);
                ShowMenu(true);
            }
        }
    }

    public void QuitGame()
    {
        Application.Quit();
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#endif
    }

    public void StartGame(string sceneName)
    {
        LoadScene(sceneName);
    }

    void LoadScene(string scene)
    {
        if (loadingScreenPrefab != null)
        {
            loadingScreenCreated = Instantiate(loadingScreenPrefab).GetComponent<LoadingScreen>();
            DontDestroyOnLoad(loadingScreenCreated);
            loadingScreenCreated.loadingOperation = SceneManager.LoadSceneAsync(scene);
        }
        else
        {
            SceneManager.LoadScene(scene);
        }
    }

    public void ShowMenu(bool value)
    {
        if (menuObj != null)
        {
            menuObj.SetActive(value);
        }
    }

    public void ShowTitle(bool value)
    {
        if (titleObj != null)
        {
            titleObj.SetActive(value);
            titleShowing = value;
        }
    }

    public void ShowControls(bool value)
    {
        if (controlsObj != null)
        {
            controlsObj.SetActive(value);
        }
    }
}