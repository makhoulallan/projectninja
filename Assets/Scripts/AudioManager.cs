﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource musicSource;
    public AudioSource effectsSource;
    float musicVolume, effectsVolume;

    void Start()
    {
        if(musicSource != null)
            musicVolume = musicSource.volume;

        if (effectsSource != null)
            effectsVolume = effectsSource.volume;
    }

    public void MuteAudio(bool value)
    {
        if(value)
        {
            if (musicSource != null)
                musicSource.volume = 0;
            if (effectsSource != null)
                effectsSource.volume = 0;
        }
        else
        {
            if (musicSource != null)
                musicSource.volume = musicVolume;
            if (effectsSource != null)
                effectsSource.volume = effectsVolume;
        }
    }
}
