﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public string damageTag = "Enemy";
    public float speed;
    public Vector3 direction;
    public float distance;
    public float damage;

    Vector3 startPosition;

    void Start()
    {
        startPosition = transform.position;
    }

    void Update()
    {
        Vector3 curPos = transform.position;
        Vector3 delta = direction * speed * Time.deltaTime;
        curPos += delta;
        transform.position = curPos;

        if(Vector3.Distance(curPos, startPosition) >= distance)
        {
            EndProjectile();
        }
    }

    void EndProjectile()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Player" && other.tag != "ProjectileIgnore")
        {
            if (other.tag == damageTag)
            {
                Unit u = other.gameObject.GetComponent<Unit>();
                u.ReceiveDamage(damage);
            }

            EndProjectile();
        }
    }
}
