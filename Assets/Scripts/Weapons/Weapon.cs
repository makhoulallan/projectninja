﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Weapon", menuName = "Weapon", order = 100)]
public class Weapon : ScriptableObject
{
    public bool isCloseRange = true;
    public float attackRange = 1.0f;
    [Tooltip("Time to wait between attacks")]
    public float attackRate = 1.0f;
    public float damage = 1;
    public Sprite weaponSprite;
    public GameObject weaponPrefab;
    public AnimationClip weaponAnimation;    
}