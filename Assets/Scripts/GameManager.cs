﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class GameManager : MonoBehaviour
{
    public UIManager uiManager;
    public AudioManager audioManager;
    public Level levelSelect;
    public List<Level> allLevels = new List<Level>();
    public Player player;
    public GameObject loadingScreenPrefab;
    LoadingScreen loadingScreenCreated;

    Level currentLevel;

    void Start()
    {
        player.transform.SetPositionAndRotation(levelSelect.startPosition.position, levelSelect.startPosition.rotation);
        currentLevel = levelSelect;
    }

    void Update()
    {
        
    }

    Level levelToLoad;
    public void ShowLevel(int index)
    {
        if (index == -1)
        {
            levelToLoad = null;
            uiManager.ShowLeaveLevel(true);
            PauseGame(true);
        }
        else
        {
            levelToLoad = allLevels[index];
            uiManager.ShowLevelInfo(true);
            uiManager.levelName.text = levelToLoad.name;
            PauseGame(true);
        }
    }

    public void LoadLevel(bool playerDead = false)
    {
        uiManager.ShowLevelInfo(false);
        uiManager.ShowLeaveLevel(false);

        if (levelToLoad == null || playerDead)
        {
            if (!currentLevel.IsFinished)
                currentLevel.ResetLevel();

            foreach (Level l in allLevels)
                l.gameObject.SetActive(false);

            levelSelect.gameObject.SetActive(true);
            levelToLoad = levelSelect;
            currentLevel = levelToLoad;
        }
        else
        {
            levelToLoad.gameObject.SetActive(true);
            levelSelect.gameObject.SetActive(false);
            currentLevel = levelToLoad;
        }

        player.transform.SetPositionAndRotation(levelToLoad.startPosition.position, levelToLoad.startPosition.rotation);
    }

    public void LoadScene(string scene)
    {
        if (loadingScreenPrefab != null)
        {
            loadingScreenCreated = Instantiate(loadingScreenPrefab).GetComponent<LoadingScreen>();
            DontDestroyOnLoad(loadingScreenCreated);
            loadingScreenCreated.loadingOperation = SceneManager.LoadSceneAsync(scene);
        }
        else
        {
            SceneManager.LoadScene(scene);
        }
    }

    public void PauseGame(bool isPaused)
    {
        float timeValue;

        if (isPaused)
        {
            timeValue = 0;
        }
        else
        {
            timeValue = 1;
        }

        Time.timeScale = timeValue;
    }

    public void QuitGame()
    {
        Application.Quit();
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#endif
    }
}
